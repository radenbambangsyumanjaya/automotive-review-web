@extends('master2')


@section('content')
<div class="row mx-5">
    <div class="col-2"><i class="bi bi-arrow-left-circle" style='font-size:40px;  cursor: pointer;'
            onclick="goBack()"></i></div>
    <div class="col-8">
        <h1 style="text-align:center;"> Buat Post </h1>
    </div>
    <div class="col-2"></div>
</div>

<div class="container  w-75">
<form role="form" action="/otomotif" method="POST" enctype="multipart/form-data">
    @csrf
    <div class="card-body">
        <div class="form-group mb-2">
            <label for="nama">Nama</label>
            <input type="text" class="form-control" id="nama" name="nama" placeholder="Masukkan Nama Kendaraan">
            @error('nama')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <div class="form-group mb-2">
            <label for="spesifikasi">Spesifikasi</label>
            <textarea name="spesifikasi"  class="form-control"id="spesifikasi" name="spesifikasi" placeholder="Masukkan Spesifikasi"></textarea>
            @error('spesifikasi')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <div class="form-group mb-2">
            <label for="year">Tahun</label>
            <input type="year" class="form-control" id="year" name="year" placeholder="Masukkan Tahun Kendaraan">
            @error('year')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        {{-- <div class="form-group">
            <label for="merek">Merek</label>
            <input type="merek" class="form-control" id="merek" name="merek" placeholder="Masukkan Merek Kendaraan">
            @error('merek')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div> --}}
        <div class="form-group mb-2">
            <label>Merek</label>
            <select name="merek_id" class="form-control" id="">
                <option value="">---Pilih Merek---</option>
                @foreach ($merek as $item)
                <option value="{{$item->id}}">{{$item->nama}}</option>

                @endforeach
            </select>
        </div>
        @error('merek_id')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <div class="form-group mb-2">
            <label>Kategori</label>
            <select name="kategori_id" class="form-control" id="">
                <option value="">---Pilih Kategori---</option>
                @foreach ($kategori as $item)
                <option value="{{$item->id}}">{{$item->nama_kategori}}</option>

                @endforeach
            </select>
        </div>
        @error('kategori_id')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <div class="form-group mb-2">
            <label for="foto">Foto</label>
            <input type="file" name="foto" class="form-control">
            @error('foto')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
    </div>
    <!-- /.card-body -->

    <div class="form-group mx-3">
        <button type="submit" class="btn btn-primary">Submit</button>
    </div>
</form>
    
</div>

@endsection
