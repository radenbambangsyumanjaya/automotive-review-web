@extends('master2')

<link rel="stylesheet" href="{{asset('css/styleview.css')}}">

@section('content')
<div class="row mx-5">
    <div class="col-2"><a href="/otomotif"><i class="bi bi-arrow-left-circle" style='font-size:40px;  cursor: pointer;'></i></a></div>
    <div class="col-8">
        <h1 style="text-align:center;">{{$otomotif->nama}}</h1>
    </div>
    <div class="col-2"></div>
</div>
<br>




<div class="row">
    <div class="col-1"></div>
    <div class="col-3">
        <img src="{{asset('images/'. $otomotif->foto)}}" class="image--cover1 my-2"><br>
    </div>
    <div class="col-7">
        <div class="m-2 ">
            <div class="card p-3">
                <p><b>Kategori: </b>{{$otomotif->kategori->nama_kategori}}
                    <br>
                    <b>Merek: </b>{{$otomotif->merek->nama}}
                    <br>
                    <b>Tahun: </b>{{$otomotif->year}}
                    <br>
                </p>
                <b>Spesifikasi: </b>
                <p>{{$otomotif->spesifikasi}}</p>
            </div>
            <div>
                <h2 style="margin-top: 15px">Review:</h2>

                {{-- COMMENT KITA --}}
                <div class="row my-4" style="border-bottom: solid; border-width:2px;  border-color:#D7D7D7">
                    <div class="col-1" style="margin: 0;">
                        <img src="https://ikapolban.id/wp-content/plugins/lima-custom-ikapolban/images/no-image.jpg"
                            class="image--cover2 my-2"><br>
                    </div>

                    <div class="col-11" >
                        <form action="/komentar" method="POST">
                            @csrf
                            <div class="form-group">
                                
                                <input type="hidden" value="{{$otomotif->id}}" name="otomotif_id" >
                                <textarea name="komentar" class="form-control" placeholder="Tulis reviewmu disini!"></textarea>
                                @error('komentar')
                                <div class="alert alert-danger">
                                    {{ $message}}
                                </div>
    
                                @enderror
                            </div>
                            <button type="submit" class="btn btn-primary my-3">Tambah Review</button>
                            </form>
                    </div>
                </div>
                {{-- @foreach ($otomotif->komentar as $item)
                <div class="card" >
                    <div class="card-body">
                      <h6 class="card-subtitle mb-2 text-muted">{{$item->user->name}}</h6>
                      <p class="card-text">{{$item->komentar}}</p>
                    </div>
                  </div>
                @endforeach --}}
                {{-- COMMENT ORANG LAIN --}}
                @foreach ($otomotif->komentar as $item)
                <div class="row my-4">
                    <div class="col-1" style="margin: 0;">
                        <img src="https://ikapolban.id/wp-content/plugins/lima-custom-ikapolban/images/no-image.jpg"
                            class="image--cover2 my-2"><br>
                    </div>
                    <div class="col-11">
                        <h5 class="my-2">{{$item->user->name}}</h5>
                        <div class="card">
                            <div class="card-body">
                                {{$item->komentar}}
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
    <div class="col-1"></div>
</div>

@endsection
