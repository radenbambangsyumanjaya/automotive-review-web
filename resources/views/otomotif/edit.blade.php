@extends('master2')


@section('content')
<div class="row mx-5">
    <div class="col-2"><i class="bi bi-arrow-left-circle" style='font-size:40px;  cursor: pointer;'
            onclick="goBack()"></i></div>
    <div class="col-8">
        <h1 style="text-align:center;"> Edit Post </h1>
    </div>
    <div class="col-2"></div>
</div>
<div class="container w-75" >
<form role="form" action="/otomotif/{{$otomotif->id}}" method="POST" enctype="multipart/form-data">
    @csrf
    @method("PUT")
    <div class="card-body">
        <div class="form-group mb-2">
            <label for="nama" class="mx-1 my-2">Nama</label>
            <input type="text" class="form-control" value="{{$otomotif->nama}}" name="nama" placeholder="Masukkan Nama Kendaraan">
            @error('nama')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <div class="form-group mb-2">
            <label for="spesifikasi" class="mx-1 my-2">Spesifikasi</label>
            <textarea name="spesifikasi"  class="form-control" name="spesifikasi" placeholder="Masukkan Spesifikasi" style="min-height: 7em">{{$otomotif->spesifikasi}}</textarea>
            @error('spesifikasi')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <div class="form-group mb-2">
            <label for="year" class="mx-1 my-2">Tahun</label>
            <input type="year" class="form-control" value="{{$otomotif->year}}" name="year" placeholder="Masukkan Tahun Kendaraan">
            @error('year')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        {{-- <div class="form-group">
            <label for="merek">Merek</label>
            <input type="merek" class="form-control" value="{{$otomotif->merek}}" name="merek" placeholder="Masukkan Merek Kendaraan">
            @error('merek')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div> --}}
        <div class="form-group mb-2">
            <label class="mx-1 my-2">Merek</label>
            <select name="merek_id" class="form-control" id="">
                <option value="">---Pilih Merek---</option>
                @foreach ($merek as $item)

                @if ($item->id === $otomotif->merek_id)

                <option value="{{$item->id}}" selected>{{$item->nama}}</option>
                @else
                <option value="{{$item->id}}">{{$item->nama}}</option>
                @endif

                @endforeach
            </select>
        </div>
        <div class="form-group mb-2">
            <label class="mx-1 my-2">Kategori</label>
            <select name="kategori_id" class="form-control" id="">
                <option value="">---Pilih Kategori---</option>
                @foreach ($kategori as $item)

                @if ($item->id === $otomotif->kategori_id)

                <option value="{{$item->id}}" selected>{{$item->nama_kategori}}</option>
                @else
                <option value="{{$item->id}}">{{$item->nama_kategori}}</option>
                @endif

                @endforeach
            </select>
        </div>
        @error('kategori_id')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <div class="form-group mb-2">
            <label for="foto" class="mx-1 my-2">Ubah Foto </label>
            <input type="file" name="foto" class="form-control" value="{{$otomotif->foto}}">
            <span style="color: #949494">(kosongkan foto jika tidak ingin mengubah)</span>
            @error('foto')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
    </div>
    <!-- /.card-body -->

    <div class="form-group">
        <button type="submit" class="btn btn-primary mx-3">Submit</button>
    </div>
</form>
</div>

@endsection
