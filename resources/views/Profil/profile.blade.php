@extends('master2')

<link rel="stylesheet" href="{{asset('css/styleprofile.css')}}">


@section('content')

    <div class="text-center">
        <div class="row">
            <div class="col-2" ><a href="/otomotif"><i class="bi bi-arrow-left-circle" style='font-size:40px;  cursor: pointer;'></i></a></div>
            <div class="col-8" ><h1>Profile</h1></div>
            <div class="col-2"></div>
        </div>
        <img src="https://ikapolban.id/wp-content/plugins/lima-custom-ikapolban/images/no-image.jpg" class="image--cover my-2"><br>
        <h2>{{ Auth::user()->name }}</h2>
        <p>{{ $profil->umur}} Tahun</p>

        <div class="card p-2" style="margin-right:25%; margin-left:25%;">
            {{$profil->bio}}
        </div>

        <a href="/profile/{{$profil->id}}/edit">
            <button type="button" class="btn btn-outline-dark my-4 px-5">Edit</button>
        </a>



    </div>

@endsection
