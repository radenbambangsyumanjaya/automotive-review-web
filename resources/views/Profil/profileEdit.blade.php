@extends('master2')



@section('content')
<div class="row mx-5">
    <div class="col-2" ><i class="bi bi-arrow-left-circle" style='font-size:40px;  cursor: pointer;' onclick="goBack()"></i></div>
    <div class="col-8"><h1 style="text-align:center;"> Edit Profile </h1></div>
    <div class="col-2"></div>
</div>
<form action="/profile/{{ $profil->id}}" method="POST">
    @csrf
    @method('PUT')

    <div class="container w-50">

      <br>

      <div class="mb-3">
        <label>Nama</label>
        <input type="text" value="{{$profil->user->name}}"  class="form-control" name="name" placeholder="">
      </div>
      <div class="mb-3">
        <label>Umur</label>
        <input type="number" value="{{$profil->umur}}" class="form-control" name="umur" placeholder="">
      </div>
      <div class="mb-3">
        <label>Bio</label>
        <textarea class="form-control" name="bio" rows="3">{{$profil->bio}}</textarea>
      </div>
      <div class="mb-3 d-flex justify-content-end" >
      <button type="submit" class="btn btn-outline-primary aaa" >Submit</button>
        </div>
    </div>

</form>
@endsection
