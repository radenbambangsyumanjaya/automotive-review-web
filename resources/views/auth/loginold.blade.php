@extends('master')

<link rel="stylesheet" href="{{asset('css/stylelogin.css')}}">

@section('content')
<body>

  <form action="/forum" method="POST">


      <div class="container">
        <h1 style="text-align:center;"> Login </h1>
        <br><br>

        <label for="email"><b>Email</b></label>
        <input type="text" placeholder="Masukkan Email" name="email" required>

        <label for="password"><b>Password</b></label>
        <input type="password" placeholder="Masukkan Password" name="password" required>


        <button type="submit " class="bg-dark my-4" style="color: white">Login</button>
        <p style="text-align:right;">Belum punya akun? silahkan <a href="/register">register</a></p>
      </div>

      <div class="container" style="background-color:#f1f1f1">
        <a href="/"><button type="button" class="cancelbtn">Cancel</button></a>
        <a href="/forum"><button type="button" class="cancelbtn btn-primary">Lanjut ke forum{nanti ini di hapus}</button></a>
      </div>

    </form>
</body>
@endsection

@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Login') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('login') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-6 offset-md-4">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                    <label class="form-check-label" for="remember">
                                        {{ __('Remember Me') }}
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Login') }}
                                </button>

                                @if (Route::has('password.request'))
                                    <a class="btn btn-link" href="{{ route('password.request') }}">
                                        {{ __('Forgot Your Password?') }}
                                    </a>
                                @endif
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
