@extends('master')

<link rel="stylesheet" href="{{asset('css/stylelogin.css')}}">

@section('content')
<body>

  <form action="#" method="post">


      <div class="container">
        <h1 style="text-align:center;"> Register </h1>
        <br><br>

        <label for="username"><b>Username</b></label>
        <input type="text" placeholder="Buat username" name="username" required>
        <label for="email"><b>Email</b></label>
        <input type="email" placeholder="Masukkan email" name="email" required>

        <label for="password"><b>Password</b></label>
        <input type="password" placeholder="Buat password" name="password" required>

        <label for="confirm_password"><b>Konfirmasi Password</b></label>
        <input type="password" placeholder="Masukkan kembali password" name="confirm_password" required>


        <button type="submit" class="bg-dark my-4" style="color: white">Register</button>
        <p style="text-align:right;">Sudah punya akun? silahkan <a href="/login">login</a></p>
      </div>

      <div class="container" style="background-color:#f1f1f1">
        <a href="/"><button type="button" class="cancelbtn">Cancel</button></a>

      </div>

    </form>
</body>
@endsection


@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Register') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('register') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Register') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
