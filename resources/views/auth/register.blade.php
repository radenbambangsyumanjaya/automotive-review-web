@extends('master')

<link rel="stylesheet" href="{{asset('css/stylelogin.css')}}">

@section('content')
<body>
    <h1 style="text-align:center;"> Register </h1>
    <form action="{{ route('register') }}" method="post" style="border: none">
        @csrf
        <div class="form-group mb-2">
            <h5>Username</h5>
            <input type="text" class="form-control" name="name" placeholder="Full name">
        </div>
        @error('name')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <div class="form-group mb-2">
            <h5>Email</h5>
            <input type="email" class="form-control" name="email" placeholder="Email">
        </div>
        @error('email')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <div class="form-group mb-2">
            <h5>Password</h5>
            <input type="password" name="password" class="form-control" placeholder="Password">

        </div>
        @error('password')

        <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <div class="form-group mb-2">
            <h5>Konfirmasi Password</h5>
            <input type="password" name="password_confirmation" class="form-control"
                placeholder="Retype password">
        </div>

        <div class="form-group mb-2">
            <h5>Umur</h5>
            <input type="number" name="umur" class="form-control" placeholder="Masukan Umur">
        </div>
        @error('umur')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <div class="form-group mb-2">
            <h5>Biodata</h5>
            <textarea name="bio" class="form-control" id="" placeholder="Enter Biodata"></textarea>
        </div>
        @error('bio')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror

        <div class="row">
            <div class="col-8">

            </div>
            <!-- /.col -->
            <div class="form-group">

            <button type="submit" class="bg-dark my-4" style="color: white">Register</button>
            </div>

            <!-- /.col -->
        </div>
        <p style="text-align:right;">Sudah punya akun? silahkan <a href="/login">login</a></p>
    </form>
</body>
@endsection
