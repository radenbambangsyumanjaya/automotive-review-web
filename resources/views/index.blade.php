@extends('master')

@section('content')
{{-- searchbar --}}
@include('layout.searchbar')

<div class="container-fluid">

    <div class="row">
        <div class="col-9">
        @forelse ($otomotif as $item)
            <!-- card untuk isi kontennya -->
            <div class="row card shadow-sm  mb-3" style=" position: relative; width: 96%;">
                <div class="my-3">
                    <div class="d-inline-flex" style=" width:300px;  height:210px;  ">
                        <img class="cardimg" src="{{asset('images/' . $item->foto)}}" alt="Cinque Terre">

                    </div>
                    <div class="d-inline-block m-3 p-1" style="position: absolute ;top:0%;">
                        <h1>{{$item->nama}}</h1>
                        <div class="d-inline-block" style="max-height:300px">
                            <p class="card-text">{{Str::limit($item->spesifikasi, 50)}}</p>
                        </div>

                    </div>
                    <div class="d-inline-block">
                        <form action="/otomotif/{{$item->id}}" method="POST">
                            @csrf
                            @method('DELETE')
                            <a href="/otomotif/{{$item->id}}" class="btn btn-primary mb-4 btnradius px-3 mx-1;"
                                style="width: 150px;">
                                Review</a>
                            <a href="/otomotif/{{$item->id}}/edit" class="btn btn-warning mb-4 btnradius px-3 mx-1;"
                                style="width: 150px;">
                                Edit</a>
                            <input type="submit" value="Delete" class="btn btn-danger mb-4 btnradius px-3 mx-1">
                        </form>
                    </div>
                </div>
            </div>


        @empty
        <h4>Tidak Ada Post</h4>
        @endforelse
        </div>
        <div class="col-3">
            <div class="card">
                <div class="card-header">
                    <p>Customize</p>
                </div>
            <div class="card-body">
                <p>Merek</p>
        <a href="/merek" class="btn btn-primary">Masuk</a>
            </div>

            </div>

        </div>

    </div>


</div>


@endsection
