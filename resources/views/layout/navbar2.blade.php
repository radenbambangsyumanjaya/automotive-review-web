
<link rel="stylesheet" href="{{asset('css/stylenavbar2.css')}}">

{{-- navbar --}}
<nav class="navbar navbar-light bg-white fixed-top shadow p-1 mb-5 bg-body rounded static-top" >
    <div class="container my-2 " style="padding: 0 5em;">
        <div class="d-flex flex-row" >
        <a class="navbar-brand" href="/otomotif"><h2>OTO</h2></a>
        </div>
        <div class="d-flex flex-row-reverse">
            <div class="btn-group">
                <span class="dropdown-toggle drop user-select-none" data-bs-toggle="dropdown" aria-expanded="false" >{{ Auth::user()->name }} <span>&nbsp</span></span>
                <ul class="dropdown-menu dropdown-menu-end my-4 shadow-sm">
                  <li><a href="/profile"><button class="dropdown-item" type="button">Profile</button></a></li>
                  <li><a href="{{ route('logout') }}" onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();"><button class="dropdown-item text-danger" type="button">Logout</button></a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form></li>
                </ul>
              </div>
        </div>
    </div>
</nav>
