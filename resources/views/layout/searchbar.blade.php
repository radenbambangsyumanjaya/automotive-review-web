<div class="container-fluid" >
  
  <form action="/search" method="GET">
      <div class="p-1 bg-light rounded rounded-pill shadow-sm mb-4 w-25">
        <div class="input-group">
          <input type="search" name="search" placeholder="Cari..." value="{{$s}}" aria-describedby="button-addon1" class="form-control border-0 bg-light">
          <div class="input-group-append">
            <button id="button-addon1" type="submit" class="btn btn-link text-primary"><i class="bi bi-search"></i></button>
          </div>
        </div>
      </div>
  </form>
</div>
