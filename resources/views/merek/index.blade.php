@extends('master2')

@section('content')
<a href="/otomotif" class="btn btn-secondary">Kembali</a>
<div class="mx-4 my-4">
    <div class="card position-relative ">
        <div class="card-header py-3 shadow-sm">
            <h3 class="card-title position-absolute top-50 start-50 translate-middle">Table Merek</h3>


                    <div class="input-group-append">
                        <button type="submit" class="btn btn-default"><i class="fas fa-search"></i></button>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.card-header -->
        <div class="card-body table">
            @if (session('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
            @endif
            <a class="btn btn-primary right-block mb-2" href="/merek/create">Add Merek</a>
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Nama</th>
                        <th style="width : 40px">Actions</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse ($merek as $key => $mereks)
                    <tr>
                        <td> {{ $key + 1 }} </td>
                        <td> {{ $mereks -> nama }}</td>
                        <td style="display: flex">
                            <a class="btn btn-warning" href="/merek/{{$mereks->id}}/edit">Edit</a>
                            <form action="/merek/{{$mereks->id}}" method="POST">
                                @csrf
                                @method('DELETE')
                                <input type="submit" value="delete" class="btn btn-danger">
                            </form>
                        </td>
                    </tr>
                    @empty
                    <tr>
                        <td colspan="4" align="center">No Cast Data</td>
                    </tr>
                    @endforelse
                </tbody>
            </table>
        </div>
        <!-- /.card-body -->
    </div>

</div>

@endsection
