@extends('master2')


@section('content')
<div class="row mx-5">
    <div class="col-2"><i class="bi bi-arrow-left-circle" style='font-size:40px;  cursor: pointer;'
            onclick="goBack()"></i></div>
    <div class="col-8">
        <h1 style="text-align:center;"> Edit Merek </h1>
    </div>
    <div class="col-2"></div>
</div>

<form role="form" action="/merek/{{$merek->id}}" method="POST">
    @csrf
    @method("PUT")
    <div class="card p-3 w-75 my-3" style="margin-left: auto; margin-right: auto;">
    <div class="card-body">
        <div class="form-group">
            <label for="nama">Nama</label>
            <input type="text" class="form-control" value="{{$merek->nama}}" name="nama" placeholder="Masukkan Nama Kendaraan">
            @error('nama')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>

    </div>
    <!-- /.card-body -->

    <div class="form-group ms-3">
        <button type="submit" class="btn btn-primary">Submit</button>
    </div>
</div>
</form>

@endsection
