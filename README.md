
### FINAL PROJECT
# OTO - Automotive web review

<img src="https://i.imgur.com/ePMMsiO.png" >

## Informasi Kelompok
### Kelompok 5:
- Achmad Rizky Maulidi
- Raden Bambang Syumanjaya

## Tema Project
Web Review Otomotif

## ERD
<img src="https://i.imgur.com/xWnt6sL.png" >

## Link Video Demo
Berikut adalah link video demo:  <br>
https://drive.google.com/file/d/1etNKO7Otm5D0kjs6GoUfjG7EKocI0b-r/view?usp=sharing

## Link Deploy Heroku:
Berikut adalah Web nya: <br>
http://otomotive.herokuapp.com/

