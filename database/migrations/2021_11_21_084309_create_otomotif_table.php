<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOtomotifTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('otomotif', function (Blueprint $table) {
        $table->bigIncrements('id');
            $table->string('nama' , 50);
            $table->text('spesifikasi');
            $table->integer('year');
            $table->string('foto', 255);
            $table->unsignedBigInteger('merek_id');
            $table->foreign('merek_id')->references('id')->on('merek');

            $table->unsignedBigInteger('kategori_id');
            $table->foreign('kategori_id')->references('id')->on('kategori');
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('otomotif');
    }
}
