<?php
use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/', function () {
    return view('auth.login');
});
Route::get('/register', function () {
    return view('register');
});

Route::get('/login', function () {
    return view('login');
});


Route::get('/create', function () {
    return view('create');
});
Route::get('/post', function () {
    return view('view');
});
Route::middleware(['auth'])->group(function () {
    Route::resource('otomotif', 'OtomotifController');
    Route::resource('merek', 'MerekController');
    Route::resource('profile', 'ProfileController')->only([
        'index', 'edit','update'
    ]);

    Route::resource('komentar', 'KomentarController')->only([
        'store'
    ]);

});


Route::get('/search', 'OtomotifController@search');



Auth::routes();


