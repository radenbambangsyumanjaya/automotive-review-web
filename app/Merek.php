<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Merek extends Model
{
    protected $table = 'merek';

    protected $fillable = ['nama'];

    public function otomotif()
    {
        return $this->hasMany('App\Otomotif');
    }
}
