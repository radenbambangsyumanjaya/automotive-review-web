<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Otomotif extends Model
{
    protected $table = 'otomotif';

    protected $fillable = ['nama','spesifikasi','year','foto','merek_id','kategori_id'];

    public function kategori()
    {
        return $this->belongsTo('App\Kategori');
    }

    public function komentar()
    {
        return $this->hasMany('App\Komentar');
    }

    public function merek()
    {
        return $this->belongsTo('App\Merek');
    }
}
