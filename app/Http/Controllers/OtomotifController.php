<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Otomotif;
use File;

class OtomotifController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $otomotif = Otomotif::all();
        $s = "";
        return view('otomotif.forum' , compact('otomotif','s'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $kategori = DB::table('kategori')->get();
        $merek = DB::table('merek')->get();
        return view('otomotif.create', compact('kategori','merek'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'spesifikasi' => 'required',
            'year' => 'required',
            'merek_id' => 'required',
            'kategori_id' => 'required',
            'foto' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',

        ]);

        $fotoname = time().'.'.$request->foto->extension();
        $request->foto->move(public_path('images'), $fotoname);

        $otomotif = new Otomotif;

        $otomotif->nama = $request->nama;
        $otomotif->spesifikasi = $request->spesifikasi;
        $otomotif->year = $request->year;
        $otomotif->merek_id = $request->merek_id;
        $otomotif->kategori_id = $request->kategori_id;
        $otomotif->foto = $fotoname;

        $otomotif->save();

        return redirect('/otomotif')->with('success', 'Berhasil Menambahkan Post!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $otomotif = Otomotif::findOrFail($id);

        return view('otomotif.view' , compact('otomotif'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function edit($id)
    {
        $kategori = DB::table('kategori')->get();
        $merek = DB::table('merek')->get();
        $otomotif = Otomotif::findOrFail($id);

        return view('otomotif.edit', compact('kategori', 'merek','otomotif'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nama' => 'required',
            'spesifikasi' => 'required',
            'year' => 'required',
            'merek_id' => 'required',
            'kategori_id' => 'required',
            'foto' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',

        ]);
        $otomotif = Otomotif::find($id);

        if($request->has('foto')){

            $fotoname = time().'.'.$request->foto->extension();
            $request->foto->move(public_path('images'), $fotoname);


        $otomotif->nama = $request->nama;
        $otomotif->spesifikasi = $request->spesifikasi;
        $otomotif->year = $request->year;
        $otomotif->merek_id = $request->merek_id;
        $otomotif->kategori_id = $request->kategori_id;
        $otomotif->foto = $fotoname;

        }else{
        $otomotif->nama = $request->nama;
        $otomotif->spesifikasi = $request->spesifikasi;
        $otomotif->year = $request->year;
        $otomotif->merek_id = $request->merek_id;
        $otomotif->kategori_id = $request->kategori_id;
        }


        $otomotif->update();

        return redirect('/otomotif')->with('success', 'Berhasil Mengedit Post!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $otomotif = Otomotif::find($id);

        $path = "gambar/";
        File::delete($path . $otomotif->foto);
        $otomotif->delete();

        return redirect('/otomotif')->with('success', 'Post Berhasil Dihapus!');
    }


    public function search(Request $request){
        $search = $request->get('search');
        $posts = DB::table('otomotif')->select('*')->where(DB::raw('lower(nama)'), 'like', '%' . strtolower($search) . '%')->get();
        return view('otomotif.forum',['otomotif'=>$posts,'s'=>$search]);
    }
}

//where('nama','like','%'.$search.'%')->paginate(5);