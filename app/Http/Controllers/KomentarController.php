<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Komentar;

class KomentarController extends Controller
{
    public function store(Request $request){

        $komentar = new Komentar;

        $komentar->komentar = $request->komentar;
        $komentar->otomotif_id = $request->otomotif_id;
        $komentar->user_id = Auth::id();

        $komentar->save();

        return redirect()->back();
    }


}
