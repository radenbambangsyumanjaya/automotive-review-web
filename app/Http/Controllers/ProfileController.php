<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\user;
use App\Profil;

class ProfileController extends Controller
{
    public function index(){

        $profil = Profil::where('user_id', Auth::id())->first();

        return view ('Profil.profile' , compact('profil'));

    }

    public function edit(){
        $profil = Profil::where('user_id', Auth::id())->first();

        return view ('Profil.profileEdit' , compact('profil'));
    }

    public function update(Request $request, $id){


        $profil =Profil::find($id);
        $user = User::find($id);

        $user->name = $request->name;
        $profil->umur = $request->umur;
        $profil->bio = $request->bio;

        $profil->save();
        $user->save();

        return redirect('/profile');



    }
}
