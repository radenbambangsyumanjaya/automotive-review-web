<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Komentar extends Model
{
    protected $table = 'komentar';

    protected $fillable = ['komentar','user_id','otomotif_id'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function otomotif()
    {
        return $this->belongsTo('App\Otomotif');
    }
}
